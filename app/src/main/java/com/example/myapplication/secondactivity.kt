package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_secondactivity.*

class secondactivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_secondactivity)
        init()

    }

    private fun init() {
        Glide.with(this)
            .load("https://images.unsplash.com/photo-1533407411655-dcce1534c1a6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60")
            .placeholder(R.mipmap.ic_launcher)
            .into(image1)

        Glide.with(this)
            .load("https://images.unsplash.com/photo-1526695029408-c5dc730c89fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60")
            .into(image)
        Glide.with(this)
            .load("https://images.unsplash.com/photo-1554080353-a576cf803bda?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60")
            .into(image2)
        Glide.with(this)
            .load("https://external-preview.redd.it/JzuxYNVSTqyH_CmVTD6PvFD9EmJYz97VZBHQm3fKz2A.jpg?auto=webp&s=c43ea9b391a0e295cf6e903b8be2685e10c19c9b")
            .into(image3)
    }
    
}